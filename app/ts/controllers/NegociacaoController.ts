import { NegociacoesView, MensagemView } from '../views/index'
import { Negociacoes, Negociacao, NegociacaoParcial } from '../models/index'
import { domInject, throttle } from '../helpers/decorators/index'
import { NegociacaoService } from '../services/index';

export class NegociacaoController {

    @domInject('#data')
    private _inputData: JQuery;

    @domInject('#quantidade')
    private _inputQuantidade: JQuery;

    @domInject('#valor')
    private _inputValor: JQuery;
    
    private _service = new NegociacaoService();
    private _negociacoes = new Negociacoes();
    private _negociacoesView = new NegociacoesView('#negociacoesView');
    private _mensagemView = new MensagemView("#mensagemView");

    constructor() {
        this._negociacoesView.update(this._negociacoes);
    }

    @throttle(300)
    adiciona(event: Event) {
        const data = new Date(this._inputData.val().replace(/-/g, ','));
        if (!this.ehDiaUtil(data)) {
            this._mensagemView.update("Aceitamos apenas negociações em dias úteis");
            return;
        }
        
        const negociacao = new Negociacao(
            data,
            parseInt(this._inputQuantidade.val()),
            parseFloat(this._inputValor.val())
        );
        this._negociacoes.adiciona(negociacao);
        this._negociacoesView.update(this._negociacoes);
        this._mensagemView.update("Negociação adicionada!");
    }

    private ehDiaUtil(data: Date): boolean{
        return data.getDay() !== DiaDaSemana.Sabado && data.getDay() !== DiaDaSemana.Domingo
    }

    @throttle()
    importaDados() {
        this._service
            .obterNegociacoes((res: Response) => {
                if (res.ok) { return res; }
                throw new Error(res.statusText);
            })
            .then(negociacoesBuscadas => {
                const negociacoesJaImportadas = this._negociacoes.paraArray();
                let negociacoesParaImportar = negociacoesBuscadas
                    .filter(negociacao =>
                        !negociacoesJaImportadas.some(jaImportada => negociacao.ehIgual(jaImportada)));
                negociacoesParaImportar.forEach(negociacao => this._negociacoes.adiciona(negociacao));
                this._negociacoesView.update(this._negociacoes);
                this._mensagemView.update(`Dados importados: ${negociacoesParaImportar.length} negociações adicionadas`);
            })
            .catch(err =>
                this._mensagemView.update(err.message));
    }
}

enum DiaDaSemana {
    Domingo,
    Segunda,
    Terca,
    Quarta,
    Quinta,
    Sexta,
    Sabado
}