import { NegociacaoController } from './controllers/NegociacaoController'

const controller = new NegociacaoController();

// comentário removido na configuração pela configuração
// "removeComments" no tsconfig.json
$('.form').submit(controller.adiciona.bind(controller));

$('#botao-importa').click(controller.importaDados.bind(controller));
