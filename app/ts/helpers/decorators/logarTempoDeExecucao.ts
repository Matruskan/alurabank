export function logarTempoDeExecucao(emSegundos: boolean = false) {
    return function(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        const metodoOriginal = descriptor.value;
        descriptor.value = function(...args: any[]) {
            const t1 = performance.now();
            
            const resultado = metodoOriginal.apply(this, args);
            
            const t2 = performance.now();
            let unidade = 'ms';
            let divisor = 1;
            if (emSegundos) {
                unidade = 's';
                divisor = 1000.0;
            }
            console.log(`----------${propertyKey}----------`);
            console.log(`Parâmetros: ${JSON.stringify(args)}`);
            console.log(`Resultado : ${JSON.stringify(resultado)}` )
            console.log(`Tempo     : ${(t2 - t1)/divisor} ${unidade}`);
            console.log(`----------${propertyKey}----------`);

            return resultado;
        }
    }
}