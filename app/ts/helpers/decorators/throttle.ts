export function throttle(milissegundos: number = 500) {
    return function(target: any, key: string, descriptor: PropertyDescriptor) {
        const funcao = descriptor.value;
        let timer = 0;
        descriptor.value = function(...args: any[]) {
            if (event) {
                event.preventDefault();
            }
            clearInterval(timer);
            timer = setTimeout(() => funcao.apply(this, args), milissegundos);
        }
        return descriptor;
    }
}